<?php
date_default_timezone_set(getenv('timezone'));

use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule();

$capsule->addConnection([
  'driver' => getenv('db_driver'),
  'host' => getenv('db_host'),
  'database' => getenv('db_database'),
  'username' => getenv('db_user'),
  'password' => getenv('db_pass'),
  'charset' => 'utf8',
  'collation' => 'utf8_unicode_ci',
  'prefix' => '',
]);

$capsule->setAsGlobal();
$capsule->bootEloquent();
