<?php

use Phinx\Migration\AbstractMigration;

class CreateRecipes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
     public function up()
     {
         $users = $this->table('recipes');
         $users->addColumn('title', 'string')
             ->addColumn('instructions', 'text')
             ->addColumn('yield', 'string')
             ->addColumn('tags', 'string')
             ->addColumn('source', 'string')
             ->addColumn('access_level', 'integer', ['default' => '1'])
             ->addColumn('created_at', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
             ->addColumn('updated_at', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
             ->save();
     }

     public function down()
     {
         $this->dropTable('recipes');
     }
}
