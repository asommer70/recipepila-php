<?php

use Phinx\Migration\AbstractMigration;

class CreateIngredients extends AbstractMigration
{
    public function up()
    {
        // Put the $amount on the has many through table.
        $ingredients = $this->table('ingredients');
        $ingredients->addColumn('name', 'string')
                     ->addColumn('measure', 'string')
                     ->addColumn('created_at', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
                     ->addColumn('updated_at', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
                     ->save();
    }

    public function down()
    {
       $this->dropTable('ingredients');
    }
}
