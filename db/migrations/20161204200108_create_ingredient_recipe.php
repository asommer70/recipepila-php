<?php

use Phinx\Migration\AbstractMigration;

class CreateIngredientRecipe extends AbstractMigration
{
  public function up()
  {
      // Put the $amount on the has many through table.
      $table = $this->table('ingredient_recipe');
      $table->addColumn('ingredient_id', 'integer')
            ->addColumn('recipe_id', 'integer')
            ->addColumn('amount', 'float')
            ->save();
  }

  public function down()
  {
     $this->dropTable('ingredient_recipe');
  }
}
