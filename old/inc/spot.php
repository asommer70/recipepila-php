<?php

function spot($dbpass) {
    $cfg = new \Spot\Config();
    $cfg->addConnection('mysql', 'mysql://pila:'. $dbpass .'@localhost/recipepila_dev');

    $spot = new \Spot\Locator($cfg);
    return $spot;
}
