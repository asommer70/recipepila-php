<?php
include './vendor/autoload.php';
include './inc/config.php';
include './inc/spot.php';
include './models/entities/recipe.php';

error_log("Migraging recipes table.\n");

date_default_timezone_set('America/New_York');
$spot = spot($dbpass);


$mapper = $spot->mapper('Entity\Recipe');
$mapper->migrate();
