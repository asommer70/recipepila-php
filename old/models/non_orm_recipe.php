<?php

class Recipe {
  private $title;
  private $ingredients = array();
  private $instructions = array();
  private $yield;
  private $tags = array();
  private $source;

  public function __construct($title = null) {
    $this->setTitle($title);
  }

  public function __toString() {
    $output = "You are calling a ". __CLASS__ ." object with a title of ";
    $output .= $this->getTitle() ."\n";
    $output .= "It is stored in ". basename(__FILE__) ." at ". __DIR__ ."\n";
    $output .= "Class methods: \n";
    $output .= implode("\n", get_class_methods(__CLASS__));
    return $output;
  }

  public function addIngredient($item, $amount = null, $measure = null) {
    if ($amount != null && !is_float($amount) && !is_int($amount)) {
      // return "The amount must be a float: ". gettype($amount) ." is given.";
      return [false, "The amount must be a float: ". gettype($amount) ." is given."];
    }
    $this->ingredients[] = array(
      'item' => ucwords($item),
      'amount' => $amount,
      'measure' => strtolower($measure)
    );
    return [true, 'Ingredients added.'];
  }

  public function addInstruction($string) {
    $this->instructions[] = $string;
  }

  public function addTag($tag) {
    $this->tags[] = strtolower($tag);
  }

  public function setYield($yield) {
    $this->yield = $yield;
  }

  public function setSource($source) {
    $this->source = ucwords($source);
  }

  public function setTitle($title) {
    if (empty($title)) {
      $this->title = null;
    } else {
      $this->title = ucwords($title);
    }
  }

  public function getTitle() {
    return $this->title;
  }

  public function getSource() {
    return $this->source;
  }

  public function getYield() {
    return $this->yield;
  }

  public function getTags() {
    return $this->tags;
  }

  public function getInstructions() {
    return $this->instructions;
  }

  public function getIngredients() {
    return $this->ingredients;
  }

  public function displayRecipe() {
    return $this->title .' by '. $this->source ."\n";
  }
}

?>
