<?php
namespace RecipePila\Controllers;

use Foil;
use RecipePila\Models\Recipe;

class BaseController
{
    protected $foil;
    
    public function __construct()
    {

        $foil = Foil\Foil::boot(['folders' => ['main' => __DIR__ .'/../../views', 'recipes' => __DIR__ .'/../../views/recipes']]);
        $this->foil = $foil->engine();
    }
}
