<?php

namespace RecipePila\Controllers;

use RecipePila\Models\Recipe;

class RecipeController extends BaseController
{
  public function index() {
    echo $this->foil->render('index', ['recipes' => Recipe::all()->toArray()]);
  }
}
