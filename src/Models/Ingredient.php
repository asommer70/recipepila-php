<?php
namespace RecipePila\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Ingredient extends Eloquent
{
  public function recipes()
  {
      return $this->belongsToMany('RecipePila\Models\Recipe');
  }
}
