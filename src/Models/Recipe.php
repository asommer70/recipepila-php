<?php
namespace RecipePila\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Recipe extends Eloquent
{
  //
  // Make Ingredients their own table with a many-to-many relationship with Recipes.
  // Add an extra field named amount to the through table to track the amount of each ingredient for each recipe.
  // Will also make it easy to lookup recipes by an ingredient.
  //
  // public function addIngredient($item, $amount = null, $measure = null)
  // {
  //   if ($amount != null && !is_float($amount) && !is_int($amount)) {
  //     // return "The amount must be a float: ". gettype($amount) ." is given.";
  //     return [false, "The amount must be a float: ". gettype($amount) ." is given."];
  //   }
  //   $ingredients = $this->get('ingredients');
  //   $ingredients[] = array(
  //     'item' => ucwords($item),
  //     'amount' => $amount,
  //     'measure' => strtolower($measure)
  //   );
  //
  //   $this->set('ingredients', $ingredients);
  //
  //   return [true, 'Ingredients added.'];
  // }

    public function ingredients()
    {
        return $this->belongsToMany('RecipePila\Models\Ingredient')->withPivot('amount');
    }
}
