<?php
require __DIR__ .'/../vendor/autoload.php';
// // require __DIR__ .'/../public/index.php';
// $dotenv = new Dotenv\Dotenv(__DIR__ .'/../');
// $dotenv->load();
// require __DIR__ .'/../bootstrap/db.php';

use RecipePila\Models\Ingredient;

class IngredientsTest extends PHPUnit_Framework_TestCase {
  /**
   * @test
   *
   */
  public function setsNameTest() {
    $expectedName = 'flour';

    $ingredient = new Ingredient();
    $ingredient->name = 'flour';

    $this->assertEquals($expectedName, $ingredient->name);
  }

  /**
   * @test
   *
   */
  public function getsAllTest() {
    $expectedCount = 2;

    $ingredients = Ingredient::all();

    $this->assertEquals($expectedCount, $ingredients->count());
  }

}
