<?php
require __DIR__ .'/../vendor/autoload.php';
// require __DIR__ .'/../public/index.php';
$dotenv = new Dotenv\Dotenv(__DIR__ .'/../');
$dotenv->load();
require __DIR__ .'/../bootstrap/db.php';

use RecipePila\Models\Recipe;
use RecipePila\Models\Ingredient;

class RecipeTest extends PHPUnit_Framework_TestCase {
  /**
   * @test
   *
   */
  public function setsTitleTest() {
    $expectedTitle = 'Pancakes';

    $recipe = new Recipe();
    $recipe->title = 'Pancakes';

    $this->assertEquals($expectedTitle, $recipe->title);
  }

  /**
   * @test
   *
   */
  public function getsAllTest() {
    $expectedRecipeCount = 2;

    $recipes = Recipe::all();

    $this->assertEquals($expectedRecipeCount, $recipes->count());
  }

  /**
   * @test
   *
   */
  public function addIngredientToRecipeTest() {
    $expectedCount = 1;

    $recipe = Recipe::find(1);
    $flour = Ingredient::find(1);
    $recipe->ingredients()->attach($flour->id, ['amount' => 1]);

    $this->assertEquals($expectedCount, $recipe->ingredients()->count());
    $recipe->ingredients()->detach($flour->id);
  }

  /**
   * @test
   *
   */
  public function getAmountForIngredientTest() {
    $expectedAmount = 1;

    $recipe = Recipe::with('ingredients')->find(1);
    $recipe->ingredients()->attach(1, ['amount' => 1]);

    $this->assertEquals($expectedAmount, $recipe->ingredients()->first()->pivot->amount);
    $recipe->ingredients()->detach(1);
  }
}
