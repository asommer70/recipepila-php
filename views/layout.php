<!DOCTYPE html>
<html>
  <head>
    <?php $this->section('head') ?>
      <meta charset="utf-8">
      <link rel="stylesheet" href="style.css" />
      <title><?php $this->section('title') ?>Recipe Pila!<?php $this->stop() ?></title>
    <?php $this->stop() ?>
  </head>
  <body>
    <?php $this->section('content') ?>
    <?php $this->stop() ?>
  </body>
</html>
