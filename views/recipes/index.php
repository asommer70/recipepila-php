<?php $this->layout('layout') ?>

<?php $this->section('title') ?>
  Recipe Pila! | Recipes
<?php $this->replace() ?>

<?php $this->section('content') ?>
  <h3><?= $this->title ?></h3>
  <p>
    Working with layouts and such... '<?= $this->title ?>'
  </p>

  <ul>
    <?php foreach ($this->recipes as $recipe) { ?>
      <li>
        <strong><?= $recipe['title'] ?></strong>
      </li>
    <?php } ?>
  </ul>
<?php $this->append() ?>
